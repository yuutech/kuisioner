function NextPage() {
  document.getElementById("page1").style.display = "none";
  document.getElementById("page2").style.display = "block";
  document.getElementById("next1").style.display = "none";
  document.getElementById("next2").style.display = "block";
}

function NextPage2() {
  document.getElementById("page2").style.display = "none";
  document.getElementById("page3").style.display = "block";
  document.getElementById("next2").style.display = "none";
  document.getElementById("next3").style.display = "block";

  var array = [];
  $(".cbk:checked").each(function () {
    array.push($(this).val());
  });

  $("#masalah").val(array.toString());
}

function NextPage3() {
  document.getElementById("page3").style.display = "none";
  document.getElementById("page6").style.display = "block";
  document.getElementById("next3").style.display = "none";
  document.getElementById("next6").style.display = "block";

  var merkpakai = [];
  $(".merkpakai:checked").each(function () {
    merkpakai.push($(this).val());
  });

  $("#merk_skincare_sering_dipakai").val(merkpakai.toString());
}

function NextPage6() {
  document.getElementById("page6").style.display = "none";
  document.getElementById("page4").style.display = "block";
  document.getElementById("next6").style.display = "none";
  document.getElementById("next4").style.display = "block";

  var dengar = [];
  $(".dengar:checked").each(function () {
    dengar.push($(this).val());
  });

  $("#merk_skincare_sering_didengar").val(dengar.toString());
}

function Ya() {
  // document.getElementById("siangmalam").style.display = "block";
  document.getElementById("tempatbeli").style.display = "block";
  document.getElementById("budgetbulanan").style.display = "block";
  // document.getElementById("merkataubanyak").style.display = "block";
  document.getElementById("localimpor").style.display = "block";
}
function Jarang() {
  // document.getElementById("siangmalam").style.display = "none";
  document.getElementById("tempatbeli").style.display = "block";
  document.getElementById("budgetbulanan").style.display = "block";
  // document.getElementById("merkataubanyak").style.display = "block";
  document.getElementById("localimpor").style.display = "block";
}
function Belum() {
  // document.getElementById("siangmalam").style.display = "none";
  document.getElementById("tempatbeli").style.display = "none";
  document.getElementById("budgetbulanan").style.display = "none";
  // document.getElementById("merkataubanyak").style.display = "none";
  document.getElementById("localimpor").style.display = "none";
}

function Paketan() {
  document.getElementById("jenisdibutuhkan").style.display = "block";
  document.getElementById("facialwashs").style.display = "none";
  document.getElementById("toners").style.display = "none";
  document.getElementById("creams").style.display = "none";
  document.getElementById("serum").style.display = "none";
}
function Acak() {
  document.getElementById("jenisdibutuhkan").style.display = "none";
  document.getElementById("facialwashs").style.display = "block";
  document.getElementById("toners").style.display = "block";
  document.getElementById("creams").style.display = "block";
  document.getElementById("serum").style.display = "block";
}

function NextPage4() {
  document.getElementById("page4").style.display = "none";
  document.getElementById("page5").style.display = "block";
  document.getElementById("next4").style.display = "none";
  document.getElementById("send").style.display = "block";

  // console.log($(".data").serializeArray());
}

function Send() {
  var jns = [];
  $(".jns:checked").each(function () {
    jns.push($(this).val());
  });

  $("#jenis_skincare_yang_dibutuhkan").val(jns.toString());

  console.log($(".data").serializeArray());

  $.ajax({
    url: "https://api.apispreadsheets.com/data/8669/",
    type: "post",
    data: $(".data").serializeArray(),
    success: function () {
      window.location.href = "thanks.html";
    },
    error: function () {
      window.location.href = "ulang.html";
    },
  });
}
